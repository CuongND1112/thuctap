<!DOCTYPE html>
<html>
<head>
	<title>Trang Cập Nhật</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
</head>
<body>
	
	<div class="container">
	  	<h2>Thông Tin Sinh Viên</h2>
	  	<form method="POST" action="../Model/update.php">
	  		<div class="row">
				<div class="col-lg-2">
					<label class="control-label" style="position:relative; top:7px;">ID:</label>
				</div>
				<div class="col-lg-10">
					<input type="text" style="width: 400px" class="form-control" name="id" required="" value="<?php echo($_GET['id']);?>">
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-2">
					<label class="control-label" style="position:relative; top:7px;">Name:</label>
				</div>
				<div class="col-lg-10">
					<input type="text" style="width: 400px" class="form-control" name="name" required="" value="<?php echo($_GET['name']);?>">
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-2">
					<label class="control-label" style="position:relative; top:7px;">Phone:</label>
				</div>
				<div class="col-lg-10">
					<input type="text" style="width: 400px" class="form-control" name="phone" required="" value="<?php echo($_GET['phone']);?>">
				</div>
			</div>
			<br>
			<div class="row">
				<div class="col-lg-2">
					<label class="control-label" style="position:relative; top:7px;">Email:</label>
				</div>
				<div class="col-lg-10">
					<input type="text" style="width: 400px" class="form-control" name="email" required="" value="<?php echo($_GET['email']);?>">
				</div>
			</div>
			<br>
			<div style="position: fixed; left: 500px">
	    		<tr>
	    			<td>
	    				<button type="submit" class="btn btn-warning" style="width: 100px"><span class="glyphicon glyphicon-check"></span>SAVE</button>
	    			</td>
	    		</tr>
	    	</div>
	  	</form>
	  	<a href="../trangchu.php"><button type="submit" class="btn btn-primary" style="width: 100px; position:fixed; left: 610px" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>CANCEL</button></a>
</div>
</body>
</html>