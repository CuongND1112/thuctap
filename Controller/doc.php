  <script>
    function checkdelete(){
      return confirm ('Bạn có muốn xóa không?'); 
    }
  </script>

  <?php
  include("Model/connect.php");

  $data = '<table class="table" border="2">
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Phone</th>
          <th>Email</th>
          <th>Action</th>
        </tr>';
    $query = "SELECT * From sv";

    if (!$result = mysqli_query($con, $query)) 
    {
        exit(mysqli_error($con));
    }
    if(mysqli_num_rows($result) > 0)
    {
        while($row = mysqli_fetch_assoc($result))
        {
            $data .= '<tr>
            <td>'.$row['id'].'</td>
            <td>'.$row['name'].'</td>
            <td>'.$row['phone'].'</td>
            <td>'.$row['email'].'</td>
            <td>
                <a href="View/sua.php?id='.$row['id'].'&name='.$row['name'].'&phone='.$row['phone'].'&email='.$row['email'].'"><button class="btn btn-success" style="width: 100px"><span class="glyphicon glyphicon-edit"></span> UPDATE</button></a>
                <a href="Model/delete.php?id='.$row['id'].'" onclick="return checkdelete()"><button class="btn btn-danger" style="width:100px"><span class="glyphicon glyphicon-trash"></span>DELETE</button></a>
            </td>
        </tr>';
            
        }
    }
    else
    {
        $data .= '<tr><td colspan="6">Records not found!</td></tr>';
    }
 
    $data .= '</table>';
 
    echo $data;

?>